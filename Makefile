all: parser.o ep.o

parser.o: parser.tab.o lex.yy.o
	gcc -o $@ $^

parser.tab.o: parser.y
	bison -d parser.y
	gcc -c parser.tab.c

lex.yy.o: parser.l
	flex parser.l
	gcc -c lex.yy.c

ep.o: ep.rkt
	raco exe -o $@ $<

clean:
	rm -f *.o *.tab.c *.tab.h *.yy.c
