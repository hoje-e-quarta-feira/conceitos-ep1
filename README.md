# EP1

### Alunos

- 10549964 - Renan Krzsinski
- 8910368 - Ygor Sad

### Testes

Foi confeccionado um script para otimizar a execução dos testes. Para rodá-lo, basta garantir que o arquivo `tests.sh` possui permissão para leitura e execução - isso pode ser alcançado por meio do comando `chmod 500 tests.sh`. Para executar o script o comando `./tests.sh` é suficiente.
